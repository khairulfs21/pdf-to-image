<?php

namespace App\Http\Livewire;

use App\Models\Resume;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;

class Preview extends Component
{
    public $temp = '';

    public $resume;

    public $path_resume;

    public $imagePaths = [];

    public function updated()
    {
        $this->imagePaths = [];

        $this->path_resume = $this->generatePDF();

        $this->storePdfPath($this->path_resume);

        $this->resume = Resume::latest()->first();

        $pdf = new \Spatie\PdfToImage\Pdf('storage/'.$this->resume->resume_path);

        foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {
            $path = 'storage/temp/builder/'.Str::random(40).$pageNumber.'.png';
            $pdf->setPage($pageNumber)
                ->saveImage($path);

            $this->imagePaths[] = $path;
        }

    }

    public function render()
    {
        $this->resume = Resume::latest()->first();

        return view('livewire.preview');
    }

    public function generatePDF()
    {

        $pdf = Pdf::loadView('sample2', ['temp' => $this->temp]);

        $pdfContent = $pdf->output();

        Storage::put('public/pdfs/1.pdf', $pdfContent);

        return 'pdfs/1.pdf';

    }

    public function storePdfPath($pdfPath)
    {
        $pdfDocument = new Resume();
        $pdfDocument->resume_path = $pdfPath;
        $pdfDocument->save();

        return $pdfDocument;
    }
}
