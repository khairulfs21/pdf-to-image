<?php

use App\Http\Livewire\Preview;
use App\Models\Resume;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $pdf = new Spatie\PdfToImage\Pdf('sample3.pdf');

    foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {
        $path = 'storage/temp/builder/'.$pageNumber.'.jpg';
        $pdf->setPage($pageNumber)
            ->saveImage($path);

        $imagePaths[] = $path;
    }

    Resume::create([
        'image_paths' => json_encode($imagePaths),
    ]);

    $pages = $pdf->getNumberOfPages();

    return view('welcome', compact('pages'));
})->name('index');

Route::get('/preview', Preview::class);

Route::get('pdf', function () {
    return view('sample2', ['temp' => 'hensem']);
    $pdf = Pdf::loadView('sample2', ['temp' => 'hensem']);

    $pdfContent = $pdf->stream();

    return $pdfContent;
});
