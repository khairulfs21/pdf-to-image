<div>
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <input type="text" wire:model.debounce.500="temp">
            </div>
        </div>
        <div wire:loading>Loading</div>
        <div class="row">
            @foreach ($imagePaths as $image)
                <div class="col-md-6">
                    <img class="img-fluid" src="{{ $image }}" alt="">
                </div>
            @endforeach
        </div>
    </div>
